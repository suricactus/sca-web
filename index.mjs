import config from 'config';
import Koa from 'koa';
import Router from 'koa-router';
import koaJson from 'koa-json';
import koaMorgan from 'koa-morgan';
import koaSlow from 'koa-slow';
import koaCors from '@koa/cors';
import koaJwt from 'koa-jwt';
import koaRequestId from 'koa-requestid';
import koaRatelimit from 'koa-ratelimit-lru';
import koaBodyParser from 'koa-bodyparser';
import passport, { localAuthHandler } from './passport';

import scawAudit from './middlewares/audit';
import scawErrorHandler from './middlewares/errorHandler';
import scawDb from './middlewares/db';
import scawLogger from './middlewares/logger';
import scawRestRbac from './middlewares/restRbac';

const capitalize = (str) => str.replace(/(?:^|\s)\S/g, s => s.toUpperCase());

export default class App extends Koa {
  constructor ({
    dbConnection,
  }) {
    super();

    this.router = new Router({ prefix: config.get('scaw.prefix') });

    if (dbConnection) {
      this.dbConnection = dbConnection;
    }

    // Error handling
    this.use(scawErrorHandler());
    this.use(scawLogger());

    // Add request id in ctx.state.id
    this.use(koaRequestId());

    for (const middleware of config.get('scaw.enabledMiddlewares')) {
      this[`_init${capitalize(middleware)}`]();
    }

    this.bindRoutes();

    this.use(this.router.routes());
  }

  bindRoutes () {
    // please override
  }

  _initScawAudit () {
    this.use(scawAudit);
  }

  _initScawRbac () {
    this.router.use('', scawRestRbac);
  }

  _initScawDb () {
    // Connect to the database and keep reference to the DB hanler in the context
    this.use(scawDb({ dbConnection: this.dbConnection }));
  }

  _initPassport () {
    this.use(passport.initialize());
    this.router.post('/authentication', koaBodyParser(), localAuthHandler);
  }

  _initSlow () {
    const slowConfig = config.get('scaw.$slow');

    if (Array.isArray(slowConfig)) {
      slowConfig.forEach(c => this.use(koaSlow(c)));
    } else if (slowConfig) {
      this.use(koaSlow(slowConfig === true ? {} : slowConfig));
    }
  }

  // log requests, but only on dev
  // on prod should use native reverse proxy logging (e.g. nginx)
  _initMorgan () {
    this.use(koaMorgan(config.get('scaw.$morgan')));
  }

  _initRatelimit () {
    this.use(
      koaRatelimit({
        ...config.get('scaw.$ratelimit'),
        id (ctx) {
          return ctx.state &&
            ctx.state.user &&
            ctx.state.user &&
            ctx.state.user.user.login
            ? ctx.state.user.login
            : ctx.ip;
        },
      })
    );
  }

  _initJson () {
    // json as stream, also beutifies
    this.use(koaJson(config.get('scaw.$json')));
  }

  _initJwt () {
    this.use(
      koaJwt({
        ...config.get('scaw.$jwt'),
        secret: config.get('scaw.$jwtSecret'),
      }).unless({ path: [/^\/authentication/] })
    );
  }

  _initCors () {
    this.use(koaCors(config.get('scaw.$cors')));
  }
}
