import dbConnect from 'sca-db';

let db;

export default ({ dbConnection }) => async (ctx, next) => {
  db = (db || dbConnection || dbConnect());

  ctx.db = await db;

  return next();
};
