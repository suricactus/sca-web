import logger from 'sca-logger';

export default () => async (ctx, next) => {
  ctx.logger = logger;
  return next();
};
