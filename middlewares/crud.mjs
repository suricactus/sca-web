import Router from 'koa-router';
import koaBodyParser from 'koa-bodyparser';
import config from 'config';
import coalesce from '../utils/coalesce';

export const defaultMethods = {
  GET: 'managed',
  POST: 'managed',
  PUT: 'managed',
  DELETE: 'managed',
};

const parseJsonifiedQueryParams = (query, jsonifiedParams) => {
  const result = {
    ...query,
  };

  jsonifiedParams = jsonifiedParams || Object.keys(query);

  for (const param of jsonifiedParams) {
    try {
      result[ param ] = JSON.parse(query[ param ]);
    } catch (e) {
    }
  }

  return result;
};

export const pgCrudFactory = (queriesFactory, configuredMethods = {}, opts = {}) => ({
  initialize: async (ctx, next) => {
    ctx.availableMethods = Object.assign({}, defaultMethods, configuredMethods);
    ctx.queries = queriesFactory instanceof Function ? queriesFactory(ctx) : queriesFactory;

    await next();
  },

  get: async (ctx, next) => {
    if (ctx.availableMethods.GET) {
      // query may contains this params: ['limit', 'page', 'filter', 'sort'];
      const query = parseJsonifiedQueryParams(ctx.request.query);

      query.range = query.range || [];

      let limit = +coalesce(query.range[1] - query.range[0] + 1, config.get('crud.select.defaultLimit'));

      limit = isNaN(limit)
        ? config.get('crud.select.defaultLimit')
        : Math.min(limit, config.get('crud.select.maxLimit'));

      let offset = isNaN(query.range[0]) ? 0 : query.range[0];

      ctx.body = await ctx.db.query(ctx.queries.select({
        filter: query.filter,
        sort: query.sort,
        limit,
        offset,
      }));

      const totalCount = (await ctx.db.one(ctx.queries.countAll())).count;
      ctx.set('X-Total-Count', totalCount);
      ctx.set('Content-Range', `items ${offset}-${ctx.body.length}/${totalCount}`);
    }

    if (ctx.availableMethods.GET !== 'managed') {
      await next();
    }
  },

  getById: async (ctx, next) => {
    const id = ctx.params.id;

    if (ctx.availableMethods.GET) {
      try {
        ctx.body = await ctx.db.one(ctx.queries.selectOne({ id }));
      } catch (e) {
        if (e.message === 'not found') {
          ctx.status = 404;
        } else {
          throw e;
        }
      }
    }

    if (ctx.availableMethods.GET !== 'managed') {
      await next();
    }
  },

  post: async (ctx, next) => {
    if (ctx.availableMethods.POST) {
      const data = ctx.data || ctx.request.body;
      ctx.body = await ctx.db.one(ctx.queries.insertOne(data));
    }

    if (ctx.availableMethods.POST !== 'managed') {
      await next();
    }
  },

  postMulti: async (ctx, next) => {
    if (ctx.availableMethods.POST) {
      const data = ctx.data || ctx.request.body;
      ctx.body = await ctx.db.query(ctx.queries.batchInsert(data));
    }

    if (ctx.availableMethods.POST !== 'managed') {
      await next();
    }
  },

  delete: async (ctx, next) => {
    const id = ctx.params.id;

    if (ctx.availableMethods.DELETE) {
      try {
        ctx.body = await ctx.db.one(ctx.queries.removeOne(id));
      } catch (e) {
        if (e.message === 'not found') {
          ctx.status = 404;
        } else {
          throw e;
        }
      }
    }

    if (ctx.availableMethods.DELETE !== 'managed') {
      await next();
    }
  },

  deleteMulti: async (ctx, next) => {
    if (ctx.availableMethods.DELETE) {
      const ids = ctx.query.id;
      ctx.body = await ctx.db.query(ctx.queries.batchRemove(ids));
    }

    if (ctx.availableMethods.DELETE !== 'managed') {
      await next();
    }
  },

  put: async (ctx, next) => {
    const id = ctx.params.id;

    if (ctx.availableMethods.PUT) {
      const data = ctx.data || ctx.request.body;
      let modifiedEntity;
      try {
        modifiedEntity = await ctx.db.one(ctx.queries.updateOne(id, data));
      } catch (e) {
        if (e.message === 'not found') {
          ctx.status = 404;
          return;
        }

        throw e;
      }
      ctx.body = modifiedEntity;
    }

    if (ctx.availableMethods.PUT !== 'managed') {
      await next();
    }
  },
});

/**
 * Creates new CRUD API endpoints
 * @param  {Object} queriesFactory    Object containing all CRUD related DB queries
 * @param  {Object} configuredMethods Object containing automatically managed queries
 * @return {Koa}                      Koa app
 */
export default (queriesFactory, configuredMethods = {}) => {
  const router = new Router();
  const pgCrud = pgCrudFactory(queriesFactory, configuredMethods);

  // GET /
  router.get('/', pgCrud.initialize, pgCrud.get);

  // GET /:id
  router.get('/:id', pgCrud.initialize, pgCrud.getById);

  // POST /
  router.post('/', koaBodyParser(), pgCrud.initialize, pgCrud.post);

  // POST /multi
  router.post('/multi', koaBodyParser(), pgCrud.initialize, pgCrud.postMulti);

  // DELETE /
  router.delete('/:id', koaBodyParser(), pgCrud.initialize, pgCrud.delete);

  // DELETE /multi
  router.delete('/multi', koaBodyParser(), pgCrud.initialize, pgCrud.deleteMulti);

  // PUT /:id
  router.put('/:id', koaBodyParser(), pgCrud.initialize, pgCrud.put);

  return router;
};
