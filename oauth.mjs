import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import promisify from 'es6-promisify';

const signAsync = promisify.promisify(jwt.sign, jwt);
const randomBytesAsync = promisify.promisify(crypto.randomBytes, crypto);

const generateJwtId = async () => {
  try {
    let jti = await randomBytesAsync(32);
    return Promise.resolve(jti.toString('hex'));
  } catch (e) {
    return Promise.reject(e);
  }
};

export const generateTokens = async (payload, secret, opts = {}) => {
  try {
    const accessTokenId = await generateJwtId();
    const refreshTokenId = await generateJwtId();

    const accessTokenPayload = Object.assign({}, payload, { jti: accessTokenId });
    const refreshTokenPayload = Object.assign({}, {
      jti: refreshTokenId,
      ati: accessTokenId
    });

    const refreshToken = await signAsync(refreshTokenPayload, secret);
    const accessToken = await signAsync(accessTokenPayload, secret);

    return Promise.resolve({
      accessToken,
      refreshToken
    });

  } catch(e) {

    return Promise.reject(e);

  }
};