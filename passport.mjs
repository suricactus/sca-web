import config from 'config';
import passport from 'koa-passport';
import LocalStrategy from 'passport-local';
import { generateTokens } from './oauth';

// if (module.hot) {
//   module.hot.accept('./oauth', () => {});
// }

passport.use(new LocalStrategy((username, password, done) => {
  if (username === 'admin' && password === 'admin') {
    done(null, {
      username: 'admin',
      login: 'admin',
      verified: 'admin',
    }, { message: 'Success' });
  } else {
    done(null, false, { message: 'Incorrect username or password.' });
  }
}));

export const localAuthHandler = (ctx, next) => {
  return passport.authenticate('local', async (err, user, info) => {
    if (user === false) {
      ctx.status = 401;
      ctx.body = info.message;
    } else {
      try {
        const { accessToken, refreshToken } = await generateTokens({user}, config.get('jwtSecret'));
        ctx.body = {
          accessToken,
          refreshToken
        }
      } catch (e) {
        ctx.throw(500, e);
      }
    }
  })(ctx, next);
};

export default passport;